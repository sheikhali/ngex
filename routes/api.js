/*
 * Serve JSON to our AngularJS client
 */

exports.name = name; 
exports.savemasjid = savemasjid;
exports.getallmasajid = getallmasajid; 
exports.getAuthor = getAuthor;

function getallmasajid(req,res){
    var dbIn = require("../dbinclude");
    dbIn.getAllMasajid(function(err, allmasajid){
      res.json({masajid :allmasajid});
    });
  };

function getAuthor(req, res){
   
   res.json({Name : 'Sheikh Ali Akhtar'});
};

function savemasjid(req, res){
  var dbIn = require("../dbinclude");
  dbIn.addNewMoreMosque(req.body, function(){
    res.json({
      savestatus: "Save successfully"
    });
  });};

function name(req, res) {
  res.json({
  	name: 'Bob'
  });
};
