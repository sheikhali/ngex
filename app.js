/**
 * Module dependencies
 */

var express = require('express'),
  routes = require('./routes'),
  api = require('./routes/api'),
  http = require('http'),
  path = require('path'),
  dbIn = require("./dbinclude"),
  bodyParser = require("body-parser"),
  multipart = require('connect-multiparty'),
  multipartMiddleware = multipart();

var app = module.exports = express();
/**
 * Configuration
 */

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

//app.use(express.methodOverride());
app.use(express.static(path.join(__dirname, 'public')));

app.all('*',function(req,res,next)
{
    if (!req.get('Origin')) return next();

   res.header("Access-Control-Allow-Origin", "http://127.0.0.1:51055");
   res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
   res.header("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,Cache-Control");

	if ('OPTIONS' == req.method) return res.send(200);
    next();
});



//app.use(app.router);

// development only
if (app.get('env') === 'development') {
//  app.use(express.errorHandler());
}

// production only
if (app.get('env') === 'production') {
  // TODO
};

app.use(bodyParser.urlencoded({ extended: true }))
// serve index and view partials
app.get('/', routes.index);
app.get('/partials/:name', routes.partials);
app.get('/mq/sp', function (req, res) {
    //res.json({'name':'sheikh'});
    dbIn.getMosquesfromserver(function (err, dts) {
        res.json(dts);
    });
});

app.post('/savemasjid',multipartMiddleware, function(req, res){
  dbIn.addNewMoreMosque(req.body, function(){
    app.get('/', routes.index);
    //res.json("Data saved for "+req.body.mosque +"successfully");
  });
});

app.get('/alibaba/rexton', function(req,res){
		res.json("V X R Ltd.");
});

app.get('/alibaba/getmdetails', function(req,res){
	dbIn.getMosquesfromserver(function (err, dts) {res.json(dts);});
});

// JSON API
app.get('/api/name', api.name);
app.post('/api/savemasjid', api.savemasjid);
app.get('/api/getallmasajid', api.getallmasajid);
app.get('/api/author', api.getAuthor)

// redirect all others to the index (HTML5 history)
app.get('*', routes.index);

/**
 * Start Server
 */

http.createServer(app).listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});
