var express = require("express");
var fs = require("fs");
var bodyParser = require("body-parser");
var path = require("path");
var morgan = require('morgan');
var app = express();
app.use('/trump',express.static('public'));
app.use(morgan('combined'));
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// create a write stream (in append mode)
var accessLogStream = fs.createWriteStream(__dirname + '/access.log', {flags: 'a'})

// setup the logger
app.use(morgan('combined', {stream: accessLogStream}))


app.get('/', function(req, res){
	//res.send(req.param);
	res.download("work.html");
	//res.send(req.params[0]);
	//res.send(req.params["Id"]);
	//res.send("Hello World");
});

app.get('/GetFile',function(req, res){
	res.sendFile(path.join(__dirname + '/work.html'));
});

app.post('/', function(req, res){
	res.send(req.body.Name);
	//res.send(req.body);
	//res.send("Post requrest received");
});

app.put("/",function(req, res){
	res.send("put request received");
});

app.delete("/", function(req,res){
	res.send("Delete request received");
});


var server = app.listen(3000,function(){
	var host = server.address().address;
	var port = server.address().port;
	console.log("Express is running at %s %s", host,port);
});
