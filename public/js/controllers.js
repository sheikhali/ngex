'use strict';

/* Controllers */

angular.module('myApp.controllers', ['ngAnimate', 'ui.bootstrap']).
    controller('AppCtrl', function ($scope, $http, $location) {
        $http({
            method: 'GET',
            url: '/api/name'
        }).
            success(function (data, status, headers, config) {
            $scope.name = data.name;
            }).
            error(function (data, status, headers, config) {
                $scope.name = 'Error!'
    });

    $scope.getClass = function (path) {
      if ($location.path().substr(0, path.length) === path) {
        return 'active';
        } else {
          return '';
        }
      };

      $http({ method: 'GET', url: '/mq/sp' }).success(function (data, status, headers, config) {
          $scope.name2 = data[1].country;

      });

  }).controller('ShowallCtrl',function($scope,$http){	
	$scope.gridOptions = { data : {} };	
    $http( {method:'GET',url:'api/getallmasajid'})
		   .success(function(data,status, headers, config){       
			$scope.columns = [{ field: 'mosque' },{ field: 'location' }, { field: 'city' }, { field: 'state' },{ field: 'country' }, { field: 'contact' }];     		$scope.gridOptions = { columnDefs: $scope.columns,data : data.masajid, enableFiltering: true, enablePagination: true, 	enablePaginationControls: true, paginationPageSize: 10};		
      	});
    $scope.cmp = "test";
  })
.controller('AdminCtrl', function($scope, $http){
	$scope.page = "Admin";
})

  .controller('AddMasjidCtrl', function($scope, $http, $modal){
    $scope.items = ['item1', 'item2', 'item3'];
    $scope.clear = function(){
      $scope.mosque = '';
      $scope.locality = '';
      $scope.city = '';
      $scope.state = '';
      $scope.country = '';
      $scope.contact = '';
    };
    $scope.savemasjid = function(){
      var formData = {
        'mosque':$scope.mosque,
        'locality': $scope.locality,
        'city': $scope.city,
        'state': $scope.state,
        'country' : $scope.country,
        'contact' : $scope.contact
       };

      $http({
        method:'POST',
        url:'api/savemasjid',
        data: formData,
        transformRequest: function(obj) {
        	var str = [];
        	for(var p in obj) str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			return str.join("&");
    	},
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      	})
		.success(function(data,status, headers, config){
        	$scope.open();
      	});
    };

    $scope.open = function (size) {
    	var modalInstance = $modal.open({
      	animation: $scope.animationsEnabled,
      	templateUrl: 'myModalContent.html',
      	controller: 'ModalInstanceCtrl',
      	size: size,
      	resolve: { items: function () {return $scope.items;}}
    	});

    	modalInstance.result.then(function (selectedItem) {
				$scope.selected = selectedItem;}, function () {$log.info('Modal dismissed at: ' + new Date());});
  		};
  })
  
  .controller('ModalInstanceCtrl', function ($scope, $modalInstance, items) {
  $scope.items = items;
  $scope.selected = {
    item: $scope.items[0]
  };

  $scope.ok = function () {
    $modalInstance.close($scope.selected.item);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});
