'use strict';

// Declare app level module which depends on filters, and services

angular.module('myApp', [
  'myApp.controllers',
  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'ui.router',
  'ui.bootstrap',
  'ui.grid',
  'ngMessages'
]).
config(function ($stateProvider, $urlRouterProvider) {
	$urlRouterProvider
		.otherwise("/showall");
	$stateProvider
    	.state('addmasjid', { url: "/addmasjid",templateUrl: "partials/addmasjid",controller: 'AddMasjidCtrl' })
    	.state('showall', { url: "/showall", templateUrl: "partials/showall", controller: 'ShowallCtrl' })
	    .state('admin', { url: "/admin", templateUrl: "partials/admin", controller: 'AdminCtrl' });
});
